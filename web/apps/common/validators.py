from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class SpecialModelValidator:
    def __init__(self, form):
        self.form = form
        self.start = form.cleaned_data.get('start_date')
        self.end = form.cleaned_data.get('end_date')

    def minimum_duration_of_special_validator(self):
        """ checks that the duration of special is greater than minimum duration """
        minimum_duration = timezone.timedelta(hours=4)
        code = 'min_duration'
        duration = self.end - self.start
        if duration < minimum_duration:
            message = _('Ensure that duration of Special is greater than 4 hours')
            self.form.add_error('start_date', ValidationError(message, code=code))
            self.form.add_error('end_date', ValidationError(message, code=code))

    def end_date_field_validator(self):
        """ checks that the end date is greater than the start date """
        code = 'violation_rule'
        if self.start > self.end:
            message = _('Ensure that End date is greater than Start date')
            self.form.add_error('end_date', ValidationError(message, code=code))
        if self.end < timezone.now() + timezone.timedelta(minutes=60):
            message = _('Ensure that End date is one hour later than the current time')
            self.form.add_error('end_date', ValidationError(message, code=code))

    def validate(self):
        self.minimum_duration_of_special_validator()
        self.end_date_field_validator()
