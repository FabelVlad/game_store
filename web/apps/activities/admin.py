from django.contrib import admin

# Register your models here.
from apps.activities.models import Review


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    pass