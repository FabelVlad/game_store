from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.activities.views import ReviewModelViewSet

app_name = 'games'
router = DefaultRouter()
router.register('reviews', ReviewModelViewSet, basename='review')

urlpatterns = [
    path('', include(router.urls)),
]
