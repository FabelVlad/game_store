from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class VoteMixin:

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated], url_name='vote_up')
    def vote_up(self, request, **kwargs):
        obj = self.get_object()
        user = request.user
        obj.vote_up(user)
        return Response()

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def vote_down(self, request, **kwargs):
        obj = self.get_object()
        user = request.user
        obj.vote_down(user)
        return Response()

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def vote_funny(self, request, **kwargs):
        obj = self.get_object()
        user = request.user
        obj.vote_funny(user)
        return Response()

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def unvote(self, request, **kwargs):
        obj = self.get_object()
        user = request.user
        obj.unvote(user)
        return Response()
