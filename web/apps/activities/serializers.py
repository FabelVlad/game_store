from rest_framework import serializers

from apps.activities.models import Review, ReviewVote


class ReviewVoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewVote
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = Review
        fields = ('created', 'modified', 'user', 'game', 'text', )
