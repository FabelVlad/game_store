from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.games.views import GameModelViewSet, GenreModelListView, GenreModelDetailView

app_name = 'games'
router = DefaultRouter()
router.register('games', GameModelViewSet, basename='game')

urlpatterns = [
    path('genres/', GenreModelListView.as_view(), name='genre-list'),
    path('genres/<slug:slug>/', GenreModelDetailView.as_view(), name='genre-detail'),
    path('', include(router.urls)),
]
