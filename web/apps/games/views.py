from django.http import HttpResponse
from django.views.generic import DetailView, TemplateView
from django.views.generic.base import View
from rest_framework import viewsets, permissions
from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import GenericAPIView, ListAPIView, RetrieveAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.activities.mixins import VoteMixin
from apps.games.models import Developer, Publisher, Genre, Game, Image
from apps.games.serializers import DeveloperSerializer, PublisherSerializer, GenreListSerializer, GameDetailSerializer, \
    ImageSerializer, GenreDetailSerializer


class GenreModelListView(ListAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreListSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    lookup_field = 'slug'


class GenreModelDetailView(RetrieveAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreDetailSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    lookup_field = 'slug'


# list_serialiser

class GameModelViewSet(VoteMixin, ReadOnlyModelViewSet):
    queryset = Game.objects.filter(display=True)
    serializer_class = GameDetailSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    lookup_field = 'slug'
