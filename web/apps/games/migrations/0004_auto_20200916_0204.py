# Generated by Django 3.1.1 on 2020-09-15 23:04

import datetime
import django.core.validators
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0003_auto_20200916_0141'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Images',
            new_name='Image',
        ),
        migrations.RenameModel(
            old_name='Videos',
            new_name='Video',
        ),
        migrations.AlterField(
            model_name='special',
            name='start_date',
            field=models.DateTimeField(validators=[django.core.validators.MinValueValidator(datetime.datetime(2020, 9, 15, 23, 3, 55, 28007, tzinfo=utc))], verbose_name='Start date of the discount'),
        ),
    ]
