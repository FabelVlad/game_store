from django import forms

from apps.common.validators import SpecialModelValidator
from apps.games.models import Special


class SpecialModelAdminForm(forms.ModelForm):
    def clean(self):
        SpecialModelValidator(self).validate()

    class Meta:
        model = Special
        fields = '__all__'
