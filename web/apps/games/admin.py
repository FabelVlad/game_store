from django.contrib import admin

from apps.games.forms import SpecialModelAdminForm
from apps.games.models import Game, SystemRequirement, Special, Publisher, Developer, Genre, Video, \
    Price, Image


@admin.register(SystemRequirement)
class SystemRequirementAdmin(admin.ModelAdmin):
    pass


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    pass


@admin.register(Special)
class SpecialAdmin(admin.ModelAdmin):
    pass
    # form = SpecialModelAdminForm


@admin.register(Publisher)
class PublisherAdmin(admin.ModelAdmin):
    pass


@admin.register(Developer)
class DeveloperAdmin(admin.ModelAdmin):
    pass


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    readonly_fields = ('slug',)


@admin.register(Video)
class VideosAdmin(admin.ModelAdmin):
    pass


@admin.register(Image)
class ImagesAdmin(admin.ModelAdmin):
    pass


@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    pass
