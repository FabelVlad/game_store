from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from apps.games.models import Developer, Publisher, Genre, Special, Game, Image, Video, Price, SystemRequirement


class DeveloperSerializer(serializers.ModelSerializer):
    class Meta:
        model = Developer
        fields = ('title',)


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        fields = ('title',)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('image',)


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('video',)


class SpecialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Special
        fields = ('title', 'start_date', 'end_date',)


class PriceSerializer(serializers.ModelSerializer):
    special = SpecialSerializer(read_only=True)

    class Meta:
        model = Price
        fields = ('price', 'discount_price', 'special',)


class SystemRequirementSerializer(serializers.ModelSerializer):
    class Meta:
        model = SystemRequirement
        exclude = ('game',)


class GenreListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='games:genre-detail', lookup_field='slug')

    class Meta:
        model = Genre
        fields = ('title', 'url',)


class GameListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='games:game-detail', lookup_field='slug')
    genres = GenreListSerializer(read_only=True, many=True)
    price_of = PriceSerializer(read_only=True)
    reviews_count = SerializerMethodField()
    likes_count = SerializerMethodField()

    class Meta:
        model = Game
        fields = ('title', 'url', 'genres', 'price_of', 'header_image', 'status', 'reviews_count', 'likes_count',)

    @staticmethod
    def get_reviews_count(obj):
        return obj.get_reviews_count()

    @staticmethod
    def get_likes_count(obj):
        return obj.get_likes_count()


class GenreDetailSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='games:genre-detail', lookup_field='slug')
    games = GameListSerializer(read_only=True, many=True)

    class Meta:
        model = Genre
        fields = ('title', 'url', 'games',)


class GameDetailSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='games:game-detail', lookup_field='slug')
    developer = serializers.StringRelatedField()
    publisher = serializers.StringRelatedField()
    genres = GenreListSerializer(read_only=True, many=True)
    images = ImageSerializer(read_only=True, many=True)
    videos = VideoSerializer(read_only=True, many=True)
    price_of = PriceSerializer(read_only=True)
    sys_requirements = SystemRequirementSerializer(read_only=True)
    reviews_count = SerializerMethodField()
    likes_count = SerializerMethodField()
    dislikes_count = SerializerMethodField()
    funny_votes_count = SerializerMethodField()
    user_vote = SerializerMethodField()


    class Meta:
        model = Game
        fields = ('title', 'url', 'header_image', 'description', 'genres', 'release_date', 'developer', 'publisher',
                  'status', 'images', 'videos', 'price_of', 'sys_requirements', 'reviews_count', 'likes_count',
                  'dislikes_count', 'funny_votes_count', 'user_vote',)

    @staticmethod
    def get_reviews_count(obj) -> int:
        return obj.get_reviews_count()

    @staticmethod
    def get_likes_count(obj) -> int:
        return obj.get_likes_count()

    @staticmethod
    def get_dislikes_count(obj) -> int:
        return obj.get_dislikes_count()

    @staticmethod
    def get_funny_votes_count(obj) -> int:
        return obj.get_funny_votes_count()

    def get_user_vote(self, obj) -> str:
        user = self.context.get('request').user
        return obj.votes.filter(user=user).values('type')
