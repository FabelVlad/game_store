from decimal import Decimal

from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from model_utils import Choices
from model_utils.fields import StatusField
from model_utils.models import TimeStampedModel

User = get_user_model()


def get_media_file_path(instance, filename):
    game_title = None
    media_dir = None
    if isinstance(instance, Game):
        game_title = instance.title
        media_dir = 'header'
    elif isinstance(instance, Image):
        game_title = str(instance.game)
        media_dir = 'images'
    elif isinstance(instance, Video):
        game_title = str(instance.game)
        media_dir = 'videos'
    return f'games/{game_title}/{media_dir}/{filename}'


class Titler(models.Model):
    title = models.CharField(_('Title'), max_length=100, unique=True)

    class Meta:
        abstract = True
        ordering = ('title',)

    def __str__(self):
        return f'{self.title}'


class Developer(Titler, TimeStampedModel):
    """ The name of the company or developer who created the game. """


class Publisher(Titler, TimeStampedModel):
    """ The name of the company that publishes the game. """


class Genre(Titler, TimeStampedModel):
    """ Game genre """
    slug = models.SlugField(_('Slug'), unique=True)
    description = models.TextField(_('Description'), max_length=2500)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('games:genre_detail', kwargs={'genre_slug': self.slug})


class Special(Titler, TimeStampedModel):
    """ A special (promotional) offer that is valid for a certain time.
     The minimum duration of the special is 4 hours.
     The special must be extended one hour before the end.
     An hour before the end of the special, the ability to change the end date is blocked
     and the Special is considered as expired. """
    discount_percentage = models.PositiveSmallIntegerField(_('Discount percentage'),
                                                           validators=[MinValueValidator(1), MaxValueValidator(99)],
                                                           help_text='40 equal 40%, max value 99')
    start_date = models.DateTimeField(_('Start date of the discount'))
    end_date = models.DateTimeField(_('End date of the discount'))

    class Meta:
        ordering = ('end_date',)

    def __str__(self):
        return f'special offer "{self.title}" expires on the {self.end_date}'

    @classmethod
    def get_pre_expired_specials(cls):
        return cls.objects.filter(end_date__gte=timezone.now(),
                                  end_date__lte=timezone.now() + timezone.timedelta(minutes=60))

    @classmethod
    def get_expired_specials(cls):
        return cls.objects.filter(end_date__lte=timezone.now())


class Game(Titler, TimeStampedModel):
    STATUS = Choices(
        ('soon', _('Upcoming game')),
        ('available', _('Available')),
        ('out', _('Out of stock')),
    )
    slug = models.SlugField(_('Slug'), blank=True, unique=True)
    header_image = models.ImageField(_('Header image'), upload_to=get_media_file_path)
    description = models.TextField(_('Description'), max_length=4000)
    genres = models.ManyToManyField(Genre, related_name='games')
    release_date = models.DateField(_('Release date'))
    developer = models.ForeignKey(Developer, null=True, blank=True, on_delete=models.SET_NULL, related_name='games')
    publisher = models.ForeignKey(Publisher, null=True, blank=True, on_delete=models.SET_NULL, related_name='games')
    status = StatusField()
    display = models.BooleanField(_('Displaying'), default=False)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)  # todo need to fix bag (fir st == fir-st)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('games:game_detail', kwargs={'game_slug': self.slug})

    def get_reviews_count(self):
        """ returnы the number of game reviews (nested and not nested) """
        return self.reviews.count()

    def get_likes_count(self):
        """ returnы the number of game likes (votes up) """
        return self.votes.filter(type='u').count()

    def get_dislikes_count(self):
        """ returnы the number of game dislikes (votes down) """
        return self.votes.filter(type='d').count()

    def get_funny_votes_count(self):
        return self.votes.filter(type='f').count()

    def vote_up(self, user):
        self.votes.update_or_create(user=user, type='u')

    def vote_down(self, user):
        self.votes.update_or_create(user=user, type='d')

    def vote_funny(self, user):
        self.votes.update_or_create(user=user, type='f')

    def unvote(self, user):
        self.votes.filter(user=user).delete()


class Image(TimeStampedModel):
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='images')
    image = ProcessedImageField(upload_to=get_media_file_path, processors=[ResizeToFill(600, 337)],
                                format='JPEG', options={'quality': 80})

    def __str__(self):
        return str(self.image)


class Video(TimeStampedModel):
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='videos')
    video = models.FileField(_('Video'), upload_to=get_media_file_path)

    def __str__(self):
        return str(self.video)


class Price(TimeStampedModel):
    """ Price for the game. Special has lower price determination priority than manually defined
    the discount price [Price.discount_price]. """
    game = models.OneToOneField(Game, on_delete=models.CASCADE, related_name='price_of')
    price = models.DecimalField(_('Price'), decimal_places=2, max_digits=9,
                                validators=[MinValueValidator(Decimal('0.01'))], help_text='enter a value in USD')
    discount_price = models.DecimalField(_('Discount price'), null=True, blank=True, decimal_places=2, max_digits=9,
                                         validators=[MinValueValidator(Decimal('0.01'))])
    special = models.ForeignKey(Special, null=True, blank=True, on_delete=models.SET_NULL, related_name='prices')

    def __str__(self):
        return f'game "{self.game}" price: {self.price} (discount price: {self.discount_price})'

    def save(self, *args, **kwargs):
        self.discount_price = self.set_discount_price()
        super().save(*args, **kwargs)

    @staticmethod
    def calculate_discount_price(price: Decimal, discount_percentage: Decimal) -> Decimal:
        """ determines the discount price of an item as
             the current price [Price.price] * (1 - discount percentage [Special.discount_percentage] / 100).
            example: 20 * (1 - 20 / 100) = 16 new price [Price.discount_price]"""
        return price * (1 - (discount_percentage / 100))

    def get_current_price(self):
        """ returns the current price of the game taking into account the discount price """
        return self.price if self.discount_price is None else self.discount_price

    def set_discount_price(self):
        if bool(self.discount_price) is False and self.special:
            discount_price = self.calculate_discount_price(self.price, Decimal(self.special.discount_percentage))
        elif bool(self.discount_price) is False and bool(self.special) is False:
            discount_price = None
        else:
            discount_price = self.discount_price
        return discount_price

    @classmethod
    def reset_discount_prices(cls, pk: int):
        """ sets discount_price to None, pk is special instance.pk """
        return cls.objects.filter(special_id=pk).update(discount_price=None)


class SystemRequirement(models.Model):
    """ System requirements for optimal game performance. """
    REQUIRED_NETWORK_CHOICES = Choices(
        ('yes', _('Broadband Internet connection')),
        ('no', _('No Internet connection required')),
    )
    game = models.OneToOneField(Game, on_delete=models.CASCADE, related_name='sys_requirements')
    os = models.CharField(_('Operating system'), max_length=200)
    processor = models.CharField(_('Processor model'), max_length=200)
    memory = models.PositiveSmallIntegerField(_('RAM'), help_text='use gigabyte (GB) as a unit of measure')
    graphic = models.CharField(_('Graphics processor'), max_length=200)
    directx = models.CharField(_('DirectX'), max_length=200, help_text='version of DirectX')
    network = models.CharField(_('Network'), max_length=3, choices=REQUIRED_NETWORK_CHOICES,
                               help_text='specify a network connection')
    storage = models.PositiveSmallIntegerField(_('Available storage space'),
                                               help_text='use gigabyte (GB) as a unit of measure')
    additional_notes = models.TextField(_('Additional notes'), blank=True, max_length=750)

    def __str__(self):
        return f'system requirements for {self.game}'

# class GameKey(TimeStampedModel):  # todo move to other dedicated service
#     game = models.ForeignKey(Game, on_delete=models.PROTECT, related_name='game_keys')
#     key = models.CharField(_('Digital activation key of game'), max_length=200)
#     owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='game_keys_owner')
#     buyer = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL,
#                               related_name='game_keys_buyer')  # todo set customer model || not
#
#     def __str__(self):
#         return f'key for {self.game} game'
